#pragma once

#include "stdafx.h"

using Microsoft::WRL::ComPtr;

class ICommandSignature
{
public:
	ICommandSignature();
	~ICommandSignature();

	void Initialize() {};
	void Reset() {};
	void Finalize() {};

	ID3D12CommandSignature* GetRootSignature() const { return m_pRootSignature.Get(); }

private:
	ComPtr<ID3D12CommandSignature> m_pRootSignature;
	UINT m_uNumOfParameters;
	bool m_bFinalized;
};

