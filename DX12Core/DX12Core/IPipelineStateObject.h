#pragma once

#include "stdafx.h"

using Microsoft::WRL::ComPtr;

class IRootSignature;

class IPipelineStateObject
{
public:
	IPipelineStateObject();
	~IPipelineStateObject();

	void SetRootSignature(const IRootSignature& rootSignature);

	static void DestroyPSOHashMap(void);

	// Getters and Setters
	ID3D12PipelineState* GetPipelineStateObject(void) const { return m_pPSO.Get(); }

	const IRootSignature&  GetRootSignature(void) const {
		ASSERT((m_pRootSignature != nullptr));
		return *m_pRootSignature;
	}
protected:
	const IRootSignature* m_pRootSignature;
	ComPtr<ID3D12PipelineState> m_pPSO;
};

class IGraphicsPSO : public IPipelineStateObject 
{
public:
	IGraphicsPSO();
	~IGraphicsPSO();

	void SetVertexShader(ID3DBlob* pVSBlob) { m_PSODesc.VS = CD3DX12_SHADER_BYTECODE(pVSBlob); };
	void SetPixelShader(ID3DBlob* pPSBlob) { m_PSODesc.PS = CD3DX12_SHADER_BYTECODE(pPSBlob); };
	void SetHullShader(ID3DBlob* pHSBlob) { m_PSODesc.HS = CD3DX12_SHADER_BYTECODE(pHSBlob); };
	void SetDomainShader(ID3DBlob* pDSBlob) { m_PSODesc.DS = CD3DX12_SHADER_BYTECODE(pDSBlob); };
	void SetGeometryShader(ID3DBlob* pGSBlob) { m_PSODesc.GS = CD3DX12_SHADER_BYTECODE(pGSBlob); };

	void SetStreamOutputDesc(const D3D12_STREAM_OUTPUT_DESC &ssDesc);
	void SetBlendDesc(const D3D12_BLEND_DESC &blendDesc);
	void SetSampleMask(const unsigned int sampleMask);
	void SetRasterizerState(const D3D12_RASTERIZER_DESC &rasterizerDesc);
	void SetDepthStencilState(const D3D12_DEPTH_STENCIL_DESC &depthStencilState);
	void SetInputLayoutDesc(const unsigned int numElements, const D3D12_INPUT_ELEMENT_DESC* pInputElementDescs);
	void SetPrimitiveTopologyType(const D3D12_PRIMITIVE_TOPOLOGY_TYPE &primitiveTopologyType);
	void SetSampleDesc(const unsigned int MsAACount, const unsigned int MsAAQuality);
	void SetRenderTargetViewFormat(const DXGI_FORMAT* rtvFormat, const DXGI_FORMAT &dsvFormat, const unsigned int numOfRTVs);

	// Perform validation and compute a hash value for fast state block comparisons
	void Finalize();
private:
	D3D12_GRAPHICS_PIPELINE_STATE_DESC m_PSODesc;
	shared_ptr<const D3D12_INPUT_ELEMENT_DESC> m_InputLayouts;
};

class IComputePSO : public IPipelineStateObject 
{
public:
	IComputePSO();
	~IComputePSO() {};

	//void SetComputeShader(const void* binary, size_t size) { m_PSODesc.CS = CD3DX12_SHADER_BYTECODE(binary, size); }
	//void SetComputeShader(const D3D12_SHADER_BYTECODE& binary) { m_PSODesc.CS = binary; }
	void SetComputeShader(ID3DBlob* pCSBlob) { m_PSODesc.CS = CD3DX12_SHADER_BYTECODE(pCSBlob); }

	void Finalize();

private:
	D3D12_COMPUTE_PIPELINE_STATE_DESC m_PSODesc;
};