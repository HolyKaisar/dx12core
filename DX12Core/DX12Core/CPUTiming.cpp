#include "stdafx.h"
#include "CPUTiming.h"

// The time elapsed for one tick
double CPUTiming::sm_CpuTickDelta = 0.0;

CPUTiming::CPUTiming() {
	LARGE_INTEGER frequency;
	if (!QueryPerformanceFrequency(&frequency)) HelperFuncs::ErrorMsgBox(L"Can't get the cpu frequence. @[CPUTiming Class]");
	sm_CpuTickDelta = 1.0f / static_cast<double>(frequency.QuadPart);

	m_u64StartTickNum = m_u64EndTickNum = 0ull;
}

CPUTiming::~CPUTiming() {

}

uint64_t CPUTiming::GetCurrentTick() {
	LARGE_INTEGER frequency;
	if (!QueryPerformanceCounter(&frequency)) HelperFuncs::ErrorMsgBox(L"Can't get the cpu frequence. @[CPUTiming Class]");
	return static_cast<uint64_t>(frequency.QuadPart);
}

void CPUTiming::StartCpuTimer() {
	m_u64StartTickNum = GetCurrentTick();
}

void CPUTiming::EndCpuTimer() {
	m_u64EndTickNum = GetCurrentTick();
}

void CPUTiming::ResetTimer() {
	m_u64StartTickNum = 0ull;
	m_u64EndTickNum = 0ull;
}