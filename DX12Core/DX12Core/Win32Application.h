#pragma once

#include "stdafx.h"
#include "DXAppFrameWork.h"

class DXAppFrameWork;

class Win32Application
{
public:
	static int Run(DXAppFrameWork* pDXApp, HINSTANCE hInstance, int nCmdShow);
	static HWND GetHwnd() { return m_hwnd; }

protected:
	static LRESULT CALLBACK WindowProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

private:
	static HWND m_hwnd;
};

#define MAIN_FUNCTION() int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE, LPSTR, int nCmdShow)

#define CREATE_APPLICATION(app_class, window_width, window_height, appName) \
	MAIN_FUNCTION() \
	{ \
		app_class appInstance(window_width, window_height, appName); \
		return Win32Application::Run(&appInstance, hInstance, nCmdShow); \
	} 