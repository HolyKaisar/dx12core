#include "stdafx.h"
#include "IDescriptorHeap.h"
#include "GraphicsCore.h"
#include "ICommandListManager.h"

using namespace DX12GraphicsCore;

mutex IDescriptorAllocator::sm_oAllocationMutex;
vector<ComPtr<ID3D12DescriptorHeap>> IDescriptorAllocator::sm_vDescriptorHeapPool;

/*****************************************************/ 
/* IDescriptorAllocator Member Functions Definitions */
/*****************************************************/
IDescriptorAllocator::IDescriptorAllocator(D3D12_DESCRIPTOR_HEAP_TYPE type) :
	m_type(type),
	m_pCurHeap(nullptr)
{

}

ID3D12DescriptorHeap* IDescriptorAllocator::RequestNewHeap(D3D12_DESCRIPTOR_HEAP_TYPE type)
{
	lock_guard<mutex> LockGuard(sm_oAllocationMutex);

	D3D12_DESCRIPTOR_HEAP_DESC Desc;
	Desc.Type = type;
	Desc.NumDescriptors = sm_ui32NumDescriptorsPerHeap;
	Desc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE;
	Desc.NodeMask = 1;
	ComPtr<ID3D12DescriptorHeap> pHeap;
	ThrowIfFailed(DX12GraphicsCore::g_pDevice->CreateDescriptorHeap(&Desc, IID_PPV_ARGS(&pHeap)));
	sm_vDescriptorHeapPool.emplace_back(pHeap);
	return pHeap.Get();
}

D3D12_CPU_DESCRIPTOR_HANDLE IDescriptorAllocator::Allocate(uint32_t count)
{
	if (m_pCurHeap == nullptr || m_ui32RemainingFreeHandles < count) 
	{
		m_pCurHeap = RequestNewHeap(m_type);
		m_oCurHandle = m_pCurHeap->GetCPUDescriptorHandleForHeapStart();
		m_ui32RemainingFreeHandles = sm_ui32NumDescriptorsPerHeap;

		if (m_ui32DescriptorSize == 0)
		{
			m_ui32DescriptorSize = DX12GraphicsCore::g_pDevice->GetDescriptorHandleIncrementSize(m_type);
		}
	}

	D3D12_CPU_DESCRIPTOR_HANDLE ret = m_oCurHandle;
	m_oCurHandle.ptr += count * m_ui32DescriptorSize;
	m_ui32RemainingFreeHandles -= count;
	return ret;
}

/*****************************************************/ 
/* IDescriptorAllocator Member Functions Definitions */
/*****************************************************/
DescriptorHandle::DescriptorHandle()
{
	m_cpuHandle.ptr = ~0ull;
	m_gpuHandle.ptr = ~0ull;
}

DescriptorHandle::DescriptorHandle(D3D12_CPU_DESCRIPTOR_HANDLE cpuHandle) :
	m_cpuHandle(cpuHandle)
{
	m_gpuHandle.ptr = ~0ull;
}

DescriptorHandle::DescriptorHandle(D3D12_CPU_DESCRIPTOR_HANDLE cpuHandle, D3D12_GPU_DESCRIPTOR_HANDLE gpuHandle) :
	m_cpuHandle(cpuHandle),
	m_gpuHandle(gpuHandle)
{
}

/************************************************/ 
/* IDescriptorHeap Member Functions Definitions */
/************************************************/
IUserDescriptorHeap::IUserDescriptorHeap(D3D12_DESCRIPTOR_HEAP_TYPE type, uint32_t maxCount)
{
	m_HeapDesc.Type = type;
	m_HeapDesc.NumDescriptors = maxCount;
	m_HeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;
	m_HeapDesc.NodeMask = 1;
}


IUserDescriptorHeap::~IUserDescriptorHeap()
{
}

void IUserDescriptorHeap::Create(const wstring& DebugHeapName)
{
	ThrowIfFailed(DX12GraphicsCore::g_pDevice->CreateDescriptorHeap(&m_HeapDesc, IID_PPV_ARGS(m_heap.ReleaseAndGetAddressOf())));
#ifdef RELEASE
	(void)DebugHeapName;
#else
	m_heap->SetName(DebugHeapName.c_str());
#endif

	m_uiDescriptorSize = DX12GraphicsCore::g_pDevice->GetDescriptorHandleIncrementSize(m_HeapDesc.Type);
	m_uiNumFreeDescriptors = m_HeapDesc.NumDescriptors;
	m_firstHandle = DescriptorHandle(m_heap->GetCPUDescriptorHandleForHeapStart());
	m_nextFreeHandle = m_firstHandle;
}

DescriptorHandle IUserDescriptorHeap::Alloc(uint32_t count)
{
	ASSERT(HasAvailableSpace(count), "Descriptor Heap out of space. Increase heap size.");
	DescriptorHandle ret = m_nextFreeHandle;
	m_nextFreeHandle += count * m_uiDescriptorSize;
	return ret;
}

bool IUserDescriptorHeap::ValidateHandle(const DescriptorHandle& dHandle) const
{
	if (dHandle.GetCpuHandle().ptr < m_firstHandle.GetCpuHandle().ptr ||
		dHandle.GetCpuHandle().ptr >= m_firstHandle.GetCpuHandle().ptr + m_HeapDesc.NumDescriptors * m_uiDescriptorSize)
	{
		return false;
	}

	if (dHandle.GetGpuHandle().ptr - m_firstHandle.GetGpuHandle().ptr != 
		dHandle.GetCpuHandle().ptr - m_firstHandle.GetCpuHandle().ptr)
	{
		return false;
	}

	return true;
}