
// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently.

#pragma once

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN  // Exclude rarely-used stuff from Windows headers.
#endif

#ifndef _DEBUG
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
#endif

#pragma warning( disable : 4100 )

// Windows header files
#include <Windows.h>
#include <string>
#include <wrl.h>
#include <shellapi.h>
#include <iostream>
#include <fstream>
#include <ostream>
#include <sstream>
#include <cmath>
#include <cstdint>
#include <algorithm>
#include <limits>
#include <exception>
#include <memory>
#include <comdef.h>

#include <stdio.h>
#include <io.h>
#include <fcntl.h>

// Multithreading
#include <mutex>

// STL header files
#include <vector>
#include <queue>
#include <stack>
#include <map>
#include <set>
#include <unordered_map>
#include <unordered_set>

// DX12 header files
#include <d3d12.h>
#include <dxgi1_4.h>
#include <d3dcompiler.h>
#include <DirectXMath.h>
#include "d3dx12.h"

// Other third party libraries
#include "HelperFuncs.h"
#include "CPUTiming.h"

using namespace std;
using namespace DirectX;
using namespace Utilities;