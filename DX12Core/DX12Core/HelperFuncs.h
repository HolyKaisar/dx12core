#pragma once

using namespace std;
namespace Utilities
{
	class HelperFuncs
	{
	public:
		HelperFuncs();
		~HelperFuncs();

		// Align to 512 bytes
		static inline unsigned int AlignTo512Byte(int base) { return ((base + 511) & ~511); }
		// Align to 256 bytes
		static inline unsigned int AlignTo256Byte(int base) { return ((base + 255) & ~255); }
		// Align to 128 bytes
		static inline unsigned int AlignTo128Byte(int base) { return (base + 127) & ~127; }

#if defined(_DEBUG)
		static inline void UseDebugConsole() {
			AllocConsole();
			freopen("CONIN$", "r", stdin);
			freopen("CONOUT$", "w", stdout);
			freopen("CONOUT$", "w", stderr);
			setvbuf(stdout, nullptr, _IONBF, 0); // Specifies a buffer for stream
			setvbuf(stderr, nullptr, _IONBF, 0); // Specifies a buffer for stream
		}

		static inline void SetConsoleColor(int color) {
			HANDLE  hConsole;
			hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
			SetConsoleTextAttribute(hConsole, min(255, max(color, 0)));
		}

		static inline void PrintInConsole(string msg) {
			char buf[256];
			sprintf_s(buf, "%s", msg.c_str());
			printf("%s\n", buf);
		}

		static inline void PrintInConsoleSub(const char* format, ...) {
			cout << "---------------" << endl;
			char buf[256];
			va_list ap;
			va_start(ap, format);
			vsprintf_s(buf, 256, format, ap);
			printf("%s\n", buf);
		}
		// This function kind of cheat the compiler,
		// because the type of the __VA_ARGS__ is unknown
		// until we pass the actual data into this function
		static inline void PrintInConsoleSub(void) {}
#endif
		// Windows message box
		static inline void WarningMsgBox(wchar_t* msg) {
			MessageBoxW(nullptr, msg, L"Warning", MB_OK);
		}

		static inline void ErrorMsgBox(wchar_t* msg) {
			MessageBoxW(nullptr, msg, L"Error", MB_OK);
		}

		static inline void ErrorMsgBox(LPCTSTR msg) {
			MessageBoxW(nullptr, msg, L"Error", MB_OK);
		}

		// FNV (Fowler-Noll-Vol hash function. Ref: https://en.wikipedia.org/wiki/Fowler%E2%80%93Noll%E2%80%93Vo_hash_function)
		static inline size_t HashIterate(size_t next, size_t currentHash = 2166136261U) { return 16777619U * currentHash ^ next; }

		template <typename T> static inline size_t HashRange(const T* begin, const T* end, size_t initialVal = 2166136261U) {
			size_t val = initialVal;

			while (begin < end) {
				val = HashIterate((size_t)*begin++, val);
			}

			return val;
		}

		template <typename T> static inline size_t HashStateArray(const T* stateDesc, size_t count, size_t initialVal = 2166136261U) {
			static_assert((sizeof(T) & 3) == 0, "State object is not word-aligned");
			return HashRange((unsigned int*)stateDesc, (unsigned int*)(stateDesc + count), initialVal);
		}

		template <typename T> static inline size_t HashState(const T* StateDesc, size_t InitialVal = 2166136261U) {
			static_assert((sizeof(T) & 3) == 0, "State object is not word-aligned");
			return HashRange((UINT*)StateDesc, (UINT*)(StateDesc + 1), InitialVal);
		}
	};
}

// Assign a name to the object to aid with debugging.
#if defined(_DEBUG)
inline void SetName(ID3D12Object* pObject, LPCWSTR name) {
	pObject->SetName(name);
}
#else
inline void SetName(ID3D12Object*, LPCWSTR) {
}
#endif

// Naming helper for ComPtr<T>.
// Assigns the name of the variable as the name of the object.
#define NAME_D3D12_OBJECT(x) SetName(x.Get(), L#x)

#if defined(_DEBUG)
// # means you get the variable name here, e.g. x = str, #x will be str in string form
#define STRINGIFY(x) #x
#define STRINGIFY_W(x) L#x
#define STRINGIFY_BUILTIN(x) STRINGIFY(x)
// Handle windows hresult
inline void ThrowIfFailed(HRESULT hr) {
	if (FAILED(hr)) {
		_com_error err(hr);
		LPCTSTR errMsg = err.ErrorMessage();

		FormatMessage(
			FORMAT_MESSAGE_ALLOCATE_BUFFER |
			FORMAT_MESSAGE_FROM_SYSTEM |
			FORMAT_MESSAGE_IGNORE_INSERTS,
			NULL,
			GetLastError(),
			0, // Default language
			(LPTSTR)&errMsg,
			0,
			NULL
			);

		// Output to Console
		OutputDebugString(errMsg);
		OutputDebugStringA("Error Occured in " STRINGIFY_BUILTIN(__FILE__) " @ " STRINGIFY_BUILTIN(__LINE__));
		__debugbreak();
	}
}

// E.g. ASSERT(expression, string, int, int, char, etc.);
// The first parameter after experssion must be a string here
#define ASSERT(isFalse, ... ) \
	if(!(bool)(isFalse)) { \
		HelperFuncs::PrintInConsole("\nAssertion failed in " STRINGIFY_BUILTIN(__FILE__) " @ " STRINGIFY_BUILTIN(__LINE__) "\n"); \
		HelperFuncs::PrintInConsoleSub(__VA_ARGS__); \
		__debugbreak(); \
	}

#else // Release do nothing (TODO)
inline void ThrowIfFailed(HRESULT hr) {}
#define ASSERT(isFalse, ...) (void)isFalse;
#endif

#ifndef SAFE_RELEASE
#define SAFE_RELEASE(p) { if (p) { (p)->Release(); (p) = nullptr; } }
#endif