#pragma once

#include "HelperFuncs.h"
#include "Win32Application.h"

using namespace std;

class DXAppFrameWork
{
public:
	DXAppFrameWork(UINT width, UINT height, wstring name);
	virtual ~DXAppFrameWork();

	virtual void OnInit() = 0;
	virtual void OnUpdate() = 0;
	virtual void OnRenderScene() = 0;
	virtual void OnRenderUI() = 0;
	virtual void OnSizeChanged(UINT width, UINT height, bool minimized) = 0;
	virtual void OnDestroy() = 0;

	virtual LRESULT HandleMessages(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam) = 0;
	virtual void AfterWinMsgProc() = 0;

	// Event handlers can be overrided
	virtual void OnKeyDown(UINT8) {}
	virtual void OnKeyUp(UINT8) {}

	// Accessors
	UINT GetWidth() const { return m_uiWidth; }
	UINT GetHeight() const { return m_uiHeight; }
	const WCHAR* GetTitle() const { return m_title.c_str(); }

	void ParseCommandLineArgs(_In_reads_(argc) WCHAR* argv[], int argc);
	void UpdateForSizeChange(UINT clientWidth, UINT clientHeight);

protected:
	wstring GetAssetFullPath(LPCWSTR assetName);
	void GetHardwareAdapter(_In_ IDXGIFactory2* pFactory, _Outptr_result_maybenull_ IDXGIAdapter1** ppAdapter);
	void SetCustomWindowText(LPCWSTR text);

	// Viewport dimensions.
	UINT m_uiWidth;
	UINT m_uiHeight;
	float m_fAspectRatio;

	// Adapter info
	bool m_bUseWarpDevice;

private:
	// Root assets path
	wstring m_assetPath;
	wstring m_title;
};

