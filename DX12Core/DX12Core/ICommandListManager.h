#pragma once

#include "ICommandAllocatorPool.h"

class ICommandQueue
{
	friend class ICommandListManager;
public:
	ICommandQueue(D3D12_COMMAND_LIST_TYPE type);
	~ICommandQueue();

	void Create(ID3D12Device* pDevice);
	void Destroy();

	inline bool IsReady() { return m_pCommandQueue != nullptr; }

	uint64_t IncrementFence(void);
	bool IsFenceComplete(uint64_t fenceValue);
	void StallForFence(uint64_t fenceValue);
	void StallForProducer(ICommandQueue& producer);
	void WaitForFence(uint64_t fenceValue);
	void WaitForIdle(void) { WaitForFence(m_uiNextFenceValue - 1); }

	ID3D12CommandQueue* GetCommandQueue() { return m_pCommandQueue; }

	uint64_t GetNextFenceValue() { return m_uiNextFenceValue; }

private:
	uint64_t ExecuteCommandList(ID3D12CommandList* list);
	ID3D12CommandAllocator* RequestAllocator(void);
	void DiscardAllocator(uint64_t fenceValueForReset, ID3D12CommandAllocator* allocator);

	ID3D12CommandQueue* m_pCommandQueue;

	const D3D12_COMMAND_LIST_TYPE m_Type;

	ICommandAllocatorPool m_oAllocatorPool;
	mutex m_fenceMutex;
	mutex m_eventMutex;

	// Mini engine says the lifetime of these objects is managed by the descriptor cache
	ID3D12Fence* m_pFence;
	uint64_t m_uiNextFenceValue;
	uint64_t m_uiLastCompletedFenceValue;
	HANDLE m_FenceEventHandle;
};

class ICommandListManager
{
public:
	ICommandListManager();
	~ICommandListManager();

	void Create(ID3D12Device* pDevice);
	void Destroy();

	ICommandQueue& GetGraphicsQueue(void) { return m_oGraphicsQueue; }
	ICommandQueue& GetComputeQueue(void) { return m_oComputeQueue; }
	ICommandQueue& GetCopyQueue(void) { return m_oCopyQueue; }

	ICommandQueue& GetQueue(D3D12_COMMAND_LIST_TYPE type = D3D12_COMMAND_LIST_TYPE_DIRECT)
	{
		switch (type) {
		case D3D12_COMMAND_LIST_TYPE_COMPUTE:
		{
			return m_oComputeQueue;
		}
		case D3D12_COMMAND_LIST_TYPE_COPY:
		{
			return m_oCopyQueue;
		}
		default: return m_oGraphicsQueue;
		}
	}

	ID3D12CommandQueue* GetGraphicsCommandQueue() { return m_oGraphicsQueue.GetCommandQueue(); }

	void CreateNewCommandList(D3D12_COMMAND_LIST_TYPE type, ID3D12GraphicsCommandList** list, ID3D12CommandAllocator** allocator);

	bool IsFenceComplete(uint64_t fenceValue)
	{
		return GetQueue(D3D12_COMMAND_LIST_TYPE(fenceValue >> 56)).IsFenceComplete(fenceValue);
	}

	// Make the CPU wait for a fence to reach a specified value
	void WaitForFence(uint64_t fenceValue);

	// Make the CPU wait for all command queues to empty (so that the GPU is idle)
	void IdleGPU(void)
	{
		m_oGraphicsQueue.WaitForIdle();
		m_oComputeQueue.WaitForIdle();
		m_oCopyQueue.WaitForIdle();
	}

private:
	ID3D12Device* m_pDevice;

	ICommandQueue m_oGraphicsQueue;
	ICommandQueue m_oComputeQueue;
	ICommandQueue m_oCopyQueue;
};

