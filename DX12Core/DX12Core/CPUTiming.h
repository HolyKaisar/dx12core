#pragma once

class CPUTiming
{
public:
	CPUTiming();
	~CPUTiming();

	void StartCpuTimer();
	void EndCpuTimer();
	void ResetTimer();

	inline double GetTimingInSeconds() {
		return sm_CpuTickDelta * (m_u64EndTickNum - m_u64StartTickNum);
	}

	inline double GetTimingInMillisecs() {
		return sm_CpuTickDelta * (m_u64EndTickNum - m_u64StartTickNum) * 1000.0;
	}

private:
	static uint64_t GetCurrentTick();

private:
	static double sm_CpuTickDelta;

	uint64_t m_u64StartTickNum;
	uint64_t m_u64EndTickNum;
};

