
#pragma once

#include <stdint.h>

class ICommandAllocatorPool
{
public:
	ICommandAllocatorPool(D3D12_COMMAND_LIST_TYPE Type);
	~ICommandAllocatorPool();

	void Create(ID3D12Device* pDevice);
	void Destroy();

	ID3D12CommandAllocator* RequestAllocator(uint64_t completedFenceValue);
	void DiscardAllocator(uint64_t fenceValue, ID3D12CommandAllocator* allocator);

	inline size_t Size() { return m_vAllocatorPool.size(); }

private:
	const D3D12_COMMAND_LIST_TYPE m_cCommandListType;

	ID3D12Device* m_pDevice;
	vector<ID3D12CommandAllocator*> m_vAllocatorPool;
	queue<pair<uint64_t, ID3D12CommandAllocator*>> m_qReadyAllocators;
	mutex m_AllocatorMutex;
};

