#include "stdafx.h"
#include "ICommandAllocatorPool.h"

static CRITICAL_SECTION m_oCriSection;

ICommandAllocatorPool::ICommandAllocatorPool(D3D12_COMMAND_LIST_TYPE type)
	: m_cCommandListType(type),
	m_pDevice(nullptr)
{
}


ICommandAllocatorPool::~ICommandAllocatorPool()
{
	Destroy();
}

void ICommandAllocatorPool::Create(ID3D12Device* pDevice) {
	m_pDevice = pDevice;
}

void ICommandAllocatorPool::Destroy() {
	for (size_t i = 0; i < m_vAllocatorPool.size(); ++i) m_vAllocatorPool[i]->Release();

	m_vAllocatorPool.clear();
}

ID3D12CommandAllocator* ICommandAllocatorPool::RequestAllocator(uint64_t completedFenceValue) {
	lock_guard<mutex> LockGuard(m_AllocatorMutex);
	// Critical section cannot guarantee every thread can get a allocator
	ID3D12CommandAllocator* pAllocator = nullptr;

	if (!m_qReadyAllocators.empty()) {
		pair<uint64_t, ID3D12CommandAllocator*>& allocatorPair = m_qReadyAllocators.front();

		if (allocatorPair.first < completedFenceValue) {
			pAllocator = allocatorPair.second;
			ThrowIfFailed(pAllocator->Reset());
			m_qReadyAllocators.pop();
		}

		// No available allocator's were ready to be reused, create a new one
		if (pAllocator) {
			ThrowIfFailed(m_pDevice->CreateCommandAllocator(m_cCommandListType, IID_PPV_ARGS(&pAllocator)));
			wchar_t allocatorName[32];
			swprintf(allocatorName, 32, L"CommandAllocator %zu", m_vAllocatorPool.size());
			pAllocator->SetName(allocatorName);
			m_vAllocatorPool.push_back(pAllocator);
		}
	}

	return pAllocator;
}

void ICommandAllocatorPool::DiscardAllocator(uint64_t fenceValue, ID3D12CommandAllocator* allocator) {
	lock_guard<mutex> LockGuard(m_AllocatorMutex);

	// The fence value tells us if this resources can be
	// freed by the CPU
	m_qReadyAllocators.push(make_pair(fenceValue, allocator));
}