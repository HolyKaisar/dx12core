#pragma once

using Microsoft::WRL::ComPtr;
// This is an unbounded resource descriptor allocator.  It is intended to provide space for CPU-visible resource descriptors
// as resources are created.  For those that need to be made shader-visible, they will need to be copied to a UserDescriptorHeap
// or a DynamicDescriptorHeap.
class IDescriptorAllocator
{
public:
	IDescriptorAllocator(D3D12_DESCRIPTOR_HEAP_TYPE type);

	D3D12_CPU_DESCRIPTOR_HANDLE Allocate(uint32_t count);

	static void DestroyAll(void);

protected:
	static const uint32_t sm_ui32NumDescriptorsPerHeap = 256;
	static mutex sm_oAllocationMutex;
	static vector<ComPtr<ID3D12DescriptorHeap>> sm_vDescriptorHeapPool;
	static ID3D12DescriptorHeap* RequestNewHeap(D3D12_DESCRIPTOR_HEAP_TYPE type);

	D3D12_DESCRIPTOR_HEAP_TYPE m_type;
	ID3D12DescriptorHeap* m_pCurHeap;
	D3D12_CPU_DESCRIPTOR_HANDLE m_oCurHandle;
	uint32_t m_ui32DescriptorSize;
	uint32_t m_ui32RemainingFreeHandles;
};

class DescriptorHandle
{
public:
	DescriptorHandle();
	DescriptorHandle(D3D12_CPU_DESCRIPTOR_HANDLE cpuHandle);
	DescriptorHandle(D3D12_CPU_DESCRIPTOR_HANDLE cpuHandle, D3D12_GPU_DESCRIPTOR_HANDLE gpuHandle);

	DescriptorHandle operator+ (int offsetScaledByDescriptorSize) const
	{
		DescriptorHandle ret = *this;
		ret += offsetScaledByDescriptorSize;
		return ret;
	}

	void operator += (int offsetScaledByDescriptorSize)
	{
		if (m_cpuHandle.ptr != ~0ull) m_cpuHandle.ptr += offsetScaledByDescriptorSize;
		if (m_gpuHandle.ptr != ~0ull) m_gpuHandle.ptr += offsetScaledByDescriptorSize;
	}

	D3D12_CPU_DESCRIPTOR_HANDLE GetCpuHandle() const { return m_cpuHandle; }
	D3D12_GPU_DESCRIPTOR_HANDLE GetGpuHandle() const { return m_gpuHandle; }

	bool IsNull() const { return m_cpuHandle.ptr == ~0ull; }
	bool IsShaderVisible() const { return m_gpuHandle.ptr != ~0ull; }

private:
	D3D12_CPU_DESCRIPTOR_HANDLE m_cpuHandle;
	D3D12_GPU_DESCRIPTOR_HANDLE m_gpuHandle;
};

class IUserDescriptorHeap
{
public:
	IUserDescriptorHeap(D3D12_DESCRIPTOR_HEAP_TYPE type, uint32_t maxCount);
	~IUserDescriptorHeap();

	void Create(const wstring& DebugHeapName);

	bool HasAvailableSpace(uint32_t count) const { return count <= m_uiNumFreeDescriptors; }

	DescriptorHandle Alloc(uint32_t count = 1);

	DescriptorHandle GetHandleAtOffset(uint32_t offset) const { return m_firstHandle + offset * m_uiDescriptorSize; }

	bool ValidateHandle(const DescriptorHandle& dHandle) const;

	ID3D12DescriptorHeap* GetHeapPointer() const { return m_heap.Get(); }
	
private:
	ComPtr<ID3D12DescriptorHeap> m_heap;
	D3D12_DESCRIPTOR_HEAP_DESC m_HeapDesc;
	uint32_t m_uiDescriptorSize;
	uint32_t m_uiNumFreeDescriptors;
	DescriptorHandle m_firstHandle;
	DescriptorHandle m_nextFreeHandle;
};

