#include "stdafx.h"
#include "ICommandListManager.h"

/**********************************************/ 
/* ICommandQueue Member Functions Definitions */
/**********************************************/
ICommandQueue::ICommandQueue(D3D12_COMMAND_LIST_TYPE type) :
	m_Type(type),
	m_pCommandQueue(nullptr),
	m_pFence(nullptr),
	m_uiNextFenceValue((uint64_t)type << 56 | 1),
	m_uiLastCompletedFenceValue((uint64_t)type << 56),
	m_oAllocatorPool(type)
{
}

ICommandQueue::~ICommandQueue() 
{
	Destroy();
}

void ICommandQueue::Create(ID3D12Device* pDevice)
{
	ASSERT(pDevice != nullptr);
	ASSERT(!IsReady());
	ASSERT(m_oAllocatorPool.Size() == 0);

	D3D12_COMMAND_QUEUE_DESC queueDesc;
	ZeroMemory(&queueDesc, sizeof(queueDesc));
	queueDesc.Type = m_Type;
	queueDesc.NodeMask = 1;
	pDevice->CreateCommandQueue(&queueDesc, IID_PPV_ARGS(&m_pCommandQueue));

	ThrowIfFailed(pDevice->CreateFence(0, D3D12_FENCE_FLAG_NONE, IID_PPV_ARGS(&m_pFence)));
	m_pFence->SetName(L"CommandListManager::m_pFence");
	m_pFence->Signal((uint64_t)m_Type << 56);

	m_FenceEventHandle = CreateEvent(nullptr, false, false, nullptr);
	ASSERT(m_FenceEventHandle != INVALID_HANDLE_VALUE);

	m_oAllocatorPool.Create(pDevice);

	ASSERT(IsReady());
}

void ICommandQueue::Destroy() 
{
	if (m_pCommandQueue == nullptr) return;

	m_oAllocatorPool.Destroy();

	CloseHandle(m_FenceEventHandle);

	SAFE_RELEASE(m_pFence);

	SAFE_RELEASE(m_pCommandQueue);
	m_pCommandQueue = nullptr;
}

uint64_t ICommandQueue::IncrementFence(void)
{
	lock_guard<mutex> LockGuard(m_fenceMutex);
	m_pCommandQueue->Signal(m_pFence, m_uiNextFenceValue);
	m_uiNextFenceValue++;
	return m_uiNextFenceValue;
}

bool ICommandQueue::IsFenceComplete(uint64_t fenceValue)
{
	// Avoid querying the fence value by testing against the last one seen.
	// The max() is to protect against an unlikely race condition that could cause the last
	// completed fence value to regress.
	if (fenceValue > m_uiLastCompletedFenceValue)
	{
		m_uiLastCompletedFenceValue = max(m_uiLastCompletedFenceValue, m_pFence->GetCompletedValue());
	}

	return fenceValue <= m_uiLastCompletedFenceValue;
}

namespace Graphics
{
	extern ICommandListManager g_CommandManager;
}

void ICommandQueue::StallForFence(uint64_t fenceValue)
{
	ICommandQueue& producer = Graphics::g_CommandManager.GetQueue((D3D12_COMMAND_LIST_TYPE)(fenceValue >> 56));
	m_pCommandQueue->Wait(producer.m_pFence, fenceValue);
}

void ICommandQueue::StallForProducer(ICommandQueue& producer)
{
	ASSERT(producer.m_uiNextFenceValue > 0);
	m_pCommandQueue->Wait(producer.m_pFence, producer.m_uiNextFenceValue - 1);
}

void ICommandQueue::WaitForFence(uint64_t fenceValue)
{
	if (IsFenceComplete(fenceValue)) return;

	lock_guard<mutex> LockGuard(m_eventMutex);

	m_pFence->SetEventOnCompletion(fenceValue, m_FenceEventHandle);
	WaitForSingleObject(m_FenceEventHandle, INFINITE);
	m_uiLastCompletedFenceValue = fenceValue;
}

uint64_t ICommandQueue::ExecuteCommandList(ID3D12CommandList* list)
{
	lock_guard<mutex> LockGuard(m_fenceMutex);

	ThrowIfFailed(((ID3D12GraphicsCommandList*)list)->Close());

	// Execute the command list
	m_pCommandQueue->ExecuteCommandLists(1, &list);

	// Signal the next fence value (with the GPU)
	m_pCommandQueue->Signal(m_pFence, m_uiNextFenceValue);

	return m_uiNextFenceValue++;
}

ID3D12CommandAllocator* ICommandQueue::RequestAllocator(void)
{
	uint64_t completedFence = m_pFence->GetCompletedValue();

	return m_oAllocatorPool.RequestAllocator(completedFence);
}

void ICommandQueue::DiscardAllocator(uint64_t fenceValueForReset, ID3D12CommandAllocator* allocator)
{
	m_oAllocatorPool.DiscardAllocator(fenceValueForReset, allocator);
}

/****************************************************/
/* ICommandListManager Member Functions Definitions */
/****************************************************/
ICommandListManager::ICommandListManager() :
	m_pDevice(nullptr),
	m_oGraphicsQueue(D3D12_COMMAND_LIST_TYPE_DIRECT),
	m_oComputeQueue(D3D12_COMMAND_LIST_TYPE_COMPUTE),
	m_oCopyQueue(D3D12_COMMAND_LIST_TYPE_COPY)
{
}

ICommandListManager::~ICommandListManager()
{
	Destroy();
}

void ICommandListManager::Destroy()
{
	// TOOD sth in the future
}

void ICommandListManager::Create(ID3D12Device* pDevice)
{
	ASSERT(pDevice != nullptr);

	m_pDevice = pDevice;

#ifndef RELEASE
	// Prevent the GPU from overclocking or underclocking to get consistent timings
	pDevice->SetStablePowerState(TRUE);
#endif

	m_oGraphicsQueue.Create(pDevice);
	m_oComputeQueue.Create(pDevice);
	m_oCopyQueue.Create(pDevice);
}

void ICommandListManager::CreateNewCommandList(D3D12_COMMAND_LIST_TYPE type, ID3D12GraphicsCommandList** list, ID3D12CommandAllocator** allocator)
{
	ASSERT(type != D3D12_COMMAND_LIST_TYPE_BUNDLE, "Bundles are not yet supported");

	switch (type) 
	{
		case D3D12_COMMAND_LIST_TYPE_DIRECT: *allocator = m_oGraphicsQueue.RequestAllocator(); break;
		case D3D12_COMMAND_LIST_TYPE_COMPUTE: *allocator = m_oComputeQueue.RequestAllocator(); break;
		case D3D12_COMMAND_LIST_TYPE_COPY: *allocator = m_oCopyQueue.RequestAllocator(); break;
		case D3D12_COMMAND_LIST_TYPE_BUNDLE: break;
	}

	ThrowIfFailed(m_pDevice->CreateCommandList(1, type, *allocator, nullptr, IID_PPV_ARGS(list)));
	(*list)->SetName(L"CommandList");
}

void ICommandListManager::WaitForFence(uint64_t fenceValue)
{
	ICommandQueue& producer = Graphics::g_CommandManager.GetQueue((D3D12_COMMAND_LIST_TYPE)(fenceValue >> 56));
	producer.WaitForFence(fenceValue);
}