//*******************************************************
// Constants member functions explaination
// https://msdn.microsoft.com/en-us/library/07x6b05d.aspx
//*******************************************************

#pragma once

#include "stdafx.h"
#include "GraphicsCore.h"

using Microsoft::WRL::ComPtr;
using namespace DX12GraphicsCore;
using namespace Utilities;

class IRootParameter 
{
	friend class IRootSignature;
public:
	IRootParameter();
	~IRootParameter();

	void CleanUp();

	void InitAsDescriptorTable(unsigned int rangeCount, D3D12_SHADER_VISIBILITY visibility = D3D12_SHADER_VISIBILITY_ALL);

	void InitAsDescriptorRange(D3D12_DESCRIPTOR_RANGE_TYPE type, unsigned int registerId, unsigned int count, D3D12_SHADER_VISIBILITY visibility = D3D12_SHADER_VISIBILITY_ALL);

	void SetDescriptorTableRange(unsigned int rangeIdx, D3D12_DESCRIPTOR_RANGE_TYPE rangeType, unsigned int registerID, unsigned int count, unsigned int space = 0);

	void InitAsConstants(unsigned int registerId, unsigned int numDwords, D3D12_SHADER_VISIBILITY visibility = D3D12_SHADER_VISIBILITY_ALL);

	void InitAsConstantsBuffer(unsigned int registerId, D3D12_SHADER_VISIBILITY visibility = D3D12_SHADER_VISIBILITY_ALL);

	void InitAsBufferSRV(unsigned int registerId, D3D12_SHADER_VISIBILITY visibility = D3D12_SHADER_VISIBILITY_ALL);

	void InitAsBufferUAV(unsigned int registerId, D3D12_SHADER_VISIBILITY visibility = D3D12_SHADER_VISIBILITY_ALL);

	const D3D12_ROOT_PARAMETER& operator() (void) const { return m_rootParam; }
protected:
	D3D12_ROOT_PARAMETER m_rootParam;
};

// Maximum 64 DWORDS divied up amongst all root parameters.
// Root constants = 1 DWORD * NumConstants
// Root descriptor (CBV, SRV, or UAV) = 2 DWORDs each
// Descriptor table pointer = 1 DWORD
// Static samplers = 0 DWORDS (compiled into shader)
class IRootSignature
{
public:
	IRootSignature(unsigned int numOfRootParams = 0, unsigned int numOfStaticSamplers = 0);
	~IRootSignature();

	static void DestroyRootSignatureHashMap(void);

	void Reset(unsigned int numOfRootParams, unsigned int numOfStaticSamplers);

	void Finalize(D3D12_ROOT_SIGNATURE_FLAGS flags = D3D12_ROOT_SIGNATURE_FLAG_NONE);

	IRootParameter& operator[] (size_t entryIdx) {
		if (entryIdx >= m_uiNumOfParameters) HelperFuncs::ErrorMsgBox(L"Index out of boundary. @IRootSignature Class.");
		return m_pParameterLists.get()[entryIdx];
	}

	const IRootParameter& operator[] (size_t entryIdx) const {
		if (entryIdx >= m_uiNumOfParameters) HelperFuncs::ErrorMsgBox(L"Index out of boundary. @IRootSignature Class.");
		return m_pParameterLists.get()[entryIdx];
	}

	/*
	* Many applications just need a fixed set of samplers, 
	* so, I just need the static sampler as a root argument 
	*/
	void InitStaticSampler(unsigned int uiRegister, const D3D12_SAMPLER_DESC& nonStaticSamplerDesc,
		D3D12_SHADER_VISIBILITY visibility = D3D12_SHADER_VISIBILITY_ALL);

	ID3D12RootSignature* GetSignature() const { return m_pRootSignature.Get(); }

protected:

	ComPtr<ID3D12RootSignature> m_pRootSignature;
	unique_ptr<D3D12_STATIC_SAMPLER_DESC[]> m_pStaticSamplerArr;
	unique_ptr<IRootParameter[]> m_pParameterLists;
	uint32_t m_uiDescriptorTableSize[16];		// Non-sampler descriptor tables need to know their descriptor count
	bool m_bFinalized;
	unsigned int m_uiNumOfParameters;
	unsigned int m_uiNumOfStaticSamplers;
	unsigned int m_uiNumOfInitializedStaticSamplers;
};

