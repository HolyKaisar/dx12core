#include "stdafx.h"
#include "IPipelineStateObject.h"
#include "IRootSignature.h"

using Microsoft::WRL::ComPtr;
map<size_t, ComPtr<ID3D12PipelineState> > s_gGraphicsPSO;
map<size_t, ComPtr<ID3D12PipelineState> > s_gComputePSO;

static CRITICAL_SECTION m_oCriSection;

IPipelineStateObject::IPipelineStateObject() 
	: m_pRootSignature(nullptr)
{
	if(!InitializeCriticalSectionAndSpinCount(&m_oCriSection, 0x00000400)) return;
}

IPipelineStateObject::~IPipelineStateObject(){

}

void IPipelineStateObject::SetRootSignature(const IRootSignature& rootSignature) {
	m_pRootSignature = &rootSignature;
}

void IPipelineStateObject::DestroyPSOHashMap() {
	s_gGraphicsPSO.clear();
	s_gComputePSO.clear();

	DeleteCriticalSection(&m_oCriSection);
}

//****************************************//
//* Definitions of IGraphicsPSO
//****************************************//
IGraphicsPSO::IGraphicsPSO() {
	ZeroMemory(&m_PSODesc, sizeof(m_PSODesc));
	m_PSODesc.NodeMask = 1;
	m_PSODesc.SampleMask = UINT_MAX;
	m_PSODesc.SampleDesc.Count = 1;
	m_PSODesc.InputLayout.NumElements = 0;
}

IGraphicsPSO::~IGraphicsPSO() {

}

void IGraphicsPSO::SetStreamOutputDesc(const D3D12_STREAM_OUTPUT_DESC &ssDesc) { 
	m_PSODesc.StreamOutput = ssDesc;
}

void IGraphicsPSO::SetBlendDesc(const D3D12_BLEND_DESC &blendDesc) { 
	m_PSODesc.BlendState = blendDesc;
}

void IGraphicsPSO::SetSampleMask(const unsigned int sampleMask) { 
	m_PSODesc.SampleMask = sampleMask; 
}

void IGraphicsPSO::SetRasterizerState(const D3D12_RASTERIZER_DESC &rasterizerDesc) { 
	m_PSODesc.RasterizerState = rasterizerDesc; 
}

void IGraphicsPSO::SetDepthStencilState(const D3D12_DEPTH_STENCIL_DESC &depthStencilState) { 
	m_PSODesc.DepthStencilState = depthStencilState; 
}

void IGraphicsPSO::SetInputLayoutDesc(const unsigned int numElements, const D3D12_INPUT_ELEMENT_DESC* pInputElementDescs) {
	m_PSODesc.InputLayout.NumElements = numElements;

	if (numElements > 0) {
		D3D12_INPUT_ELEMENT_DESC* NewElements = (D3D12_INPUT_ELEMENT_DESC*)malloc(sizeof(D3D12_INPUT_ELEMENT_DESC) * numElements);
		memcpy(NewElements, pInputElementDescs, numElements * sizeof(D3D12_INPUT_ELEMENT_DESC));
		m_InputLayouts.reset((const D3D12_INPUT_ELEMENT_DESC*)NewElements);
	}
	else {
		m_InputLayouts = nullptr;
	}
}

void IGraphicsPSO::SetPrimitiveTopologyType(const D3D12_PRIMITIVE_TOPOLOGY_TYPE &primitiveTopologyType) {
	ASSERT(primitiveTopologyType != D3D12_PRIMITIVE_TOPOLOGY_TYPE_UNDEFINED, "Can't draw with undefined topology");
	m_PSODesc.PrimitiveTopologyType = primitiveTopologyType;
}

void IGraphicsPSO::SetSampleDesc(const unsigned int MsAACount, const unsigned int MsAAQuality) { 
	m_PSODesc.SampleDesc.Count = MsAACount; 
	m_PSODesc.SampleDesc.Quality = MsAAQuality; 
}

void IGraphicsPSO::SetRenderTargetViewFormat(const DXGI_FORMAT* rtvFormat, const DXGI_FORMAT &dsvFormat, const unsigned int numOfRTVs) {
	for (unsigned int i = 0; i < numOfRTVs; i++) m_PSODesc.RTVFormats[i] = rtvFormat[i];
	for (unsigned int i = numOfRTVs; i < m_PSODesc.NumRenderTargets; ++i) m_PSODesc.RTVFormats[i] = DXGI_FORMAT_UNKNOWN;
	
	m_PSODesc.DSVFormat = dsvFormat;
	m_PSODesc.NumRenderTargets = numOfRTVs;
}

void IGraphicsPSO::Finalize() {
	// RootSignature should be finalized first
	m_PSODesc.pRootSignature = m_pRootSignature->GetSignature();
	ASSERT((m_PSODesc.pRootSignature != nullptr));

	size_t HashCode = HelperFuncs::HashState(&m_PSODesc);
	HashCode = HelperFuncs::HashStateArray(m_InputLayouts.get(), m_PSODesc.InputLayout.NumElements, HashCode);
	m_PSODesc.InputLayout.pInputElementDescs = m_InputLayouts.get();

	ID3D12PipelineState** PSORef = nullptr;

	bool firstCompiled = false;
	{
		EnterCriticalSection(&m_oCriSection);
		auto iter = s_gGraphicsPSO.find(HashCode);

		if (iter == s_gGraphicsPSO.end()) {
			firstCompiled = true;
			PSORef = s_gGraphicsPSO[HashCode].GetAddressOf();
		}
		else {
			PSORef = iter->second.GetAddressOf();
		}
		LeaveCriticalSection(&m_oCriSection);
	}

	if (firstCompiled) {
		//ThrowIfFailed(g_pDevice->CreateGraphicsPipelineState(&m_PSODesc, IID_PPV_ARGS(&m_pPSO)));
		g_pDevice->CreateGraphicsPipelineState(&m_PSODesc, IID_PPV_ARGS(&m_pPSO));
		s_gGraphicsPSO[HashCode].Attach(m_pPSO.Get());
	}
	else {
		while (*PSORef == nullptr) {
			this_thread::yield();
		}
		m_pPSO = *PSORef;
	}
}

//**********************************
// Compute PSO Definitions
//**********************************
IComputePSO::IComputePSO() {
	ZeroMemory(&m_PSODesc, sizeof(m_PSODesc));
	m_PSODesc.NodeMask = 1;
}

void IComputePSO::Finalize() {
	// RootSignature should be finalized first
	m_PSODesc.pRootSignature = m_pRootSignature->GetSignature();
	ASSERT((m_PSODesc.pRootSignature != nullptr));

	size_t HashCode = HelperFuncs::HashState(&m_PSODesc);

	ID3D12PipelineState** PSORef = nullptr;

	bool firstCompiled = false;
	{
		EnterCriticalSection(&m_oCriSection);
		auto iter = s_gComputePSO.find(HashCode);

		if (iter == s_gComputePSO.end()) {
			firstCompiled = true;
			PSORef = s_gComputePSO[HashCode].GetAddressOf();
		}
		else {
			PSORef = iter->second.GetAddressOf();
		}
		LeaveCriticalSection(&m_oCriSection);
	}

	if (firstCompiled) {
		ThrowIfFailed(g_pDevice->CreateComputePipelineState(&m_PSODesc, IID_PPV_ARGS(&m_pPSO)));
		s_gComputePSO[HashCode].Attach(m_pPSO.Get());
	}
	else {
		while (*PSORef == nullptr) {
			this_thread::yield();
		}
		m_pPSO = *PSORef;
	}
}