#include "stdafx.h"
#include "IRootSignature.h"

static map<size_t, ComPtr<ID3D12RootSignature> > s_gRootSignatureHashMap;
static CRITICAL_SECTION s_HashMapCriSection; // For root sinature finalized

IRootSignature::IRootSignature(unsigned int numOfRootParams, unsigned int numOfStaticSamplers)
	: m_bFinalized(false),
	m_uiNumOfParameters(numOfRootParams) {
	Reset(numOfRootParams, numOfStaticSamplers);

	if (!InitializeCriticalSectionAndSpinCount(&s_HashMapCriSection, 0x00000400)) return;
}

IRootSignature::~IRootSignature(){
}

void IRootSignature::DestroyRootSignatureHashMap() {
	s_gRootSignatureHashMap.clear();
}

void IRootSignature::Reset(unsigned int numOfRootParams, unsigned int numOfStaticSamplers) {
	m_pParameterLists = nullptr;
	if (numOfRootParams > 0) m_pParameterLists.reset(new IRootParameter[numOfRootParams]);
	m_uiNumOfParameters = numOfRootParams;

	m_pStaticSamplerArr = nullptr;
	if (numOfStaticSamplers > 0) m_pStaticSamplerArr.reset(new D3D12_STATIC_SAMPLER_DESC[numOfStaticSamplers]);
	m_uiNumOfStaticSamplers = numOfStaticSamplers;
	m_uiNumOfInitializedStaticSamplers = 0;
}

void IRootSignature::InitStaticSampler(
	unsigned int uiRegister, 
	const D3D12_SAMPLER_DESC& nonStaticSamplerDesc,
	D3D12_SHADER_VISIBILITY visibility) {
	
	if (m_uiNumOfInitializedStaticSamplers < m_uiNumOfStaticSamplers) HelperFuncs::WarningMsgBox(L"Can't add more samplers.");

	D3D12_STATIC_SAMPLER_DESC& staticSamplerDesc = m_pStaticSamplerArr[m_uiNumOfInitializedStaticSamplers++];

	staticSamplerDesc.Filter = nonStaticSamplerDesc.Filter;
	staticSamplerDesc.AddressU = nonStaticSamplerDesc.AddressU;
	staticSamplerDesc.AddressV = nonStaticSamplerDesc.AddressV;
	staticSamplerDesc.AddressW = nonStaticSamplerDesc.AddressW;
	staticSamplerDesc.MipLODBias = nonStaticSamplerDesc.MipLODBias;
	staticSamplerDesc.MaxAnisotropy = nonStaticSamplerDesc.MaxAnisotropy;
	staticSamplerDesc.ComparisonFunc = nonStaticSamplerDesc.ComparisonFunc;
	staticSamplerDesc.BorderColor = D3D12_STATIC_BORDER_COLOR_OPAQUE_WHITE;
	staticSamplerDesc.MinLOD = nonStaticSamplerDesc.MinLOD;
	staticSamplerDesc.MaxLOD = nonStaticSamplerDesc.MaxLOD;
	staticSamplerDesc.ShaderRegister = uiRegister;
	staticSamplerDesc.RegisterSpace = 0;
	staticSamplerDesc.ShaderVisibility = visibility;


	if (staticSamplerDesc.AddressU == D3D12_TEXTURE_ADDRESS_MODE_BORDER ||
		staticSamplerDesc.AddressV == D3D12_TEXTURE_ADDRESS_MODE_BORDER ||
		staticSamplerDesc.AddressW == D3D12_TEXTURE_ADDRESS_MODE_BORDER) {

		if (			
			// Transparent Black
			nonStaticSamplerDesc.BorderColor[0] == 0.0f &&
			nonStaticSamplerDesc.BorderColor[1] == 0.0f &&
			nonStaticSamplerDesc.BorderColor[2] == 0.0f &&
			nonStaticSamplerDesc.BorderColor[3] == 0.0f ||
			// Opaque Black
			nonStaticSamplerDesc.BorderColor[0] == 0.0f &&
			nonStaticSamplerDesc.BorderColor[1] == 0.0f &&
			nonStaticSamplerDesc.BorderColor[2] == 0.0f &&
			nonStaticSamplerDesc.BorderColor[3] == 1.0f ||
			// Opaque White
			nonStaticSamplerDesc.BorderColor[0] == 1.0f &&
			nonStaticSamplerDesc.BorderColor[1] == 1.0f &&
			nonStaticSamplerDesc.BorderColor[2] == 1.0f &&
			nonStaticSamplerDesc.BorderColor[3] == 1.0f) {

			HelperFuncs::ErrorMsgBox(L"Sampler border color does not match static sampler limitations");
		}

		if (nonStaticSamplerDesc.BorderColor[3] == 1.0f)
		{
			if (nonStaticSamplerDesc.BorderColor[0] == 1.0f)
				staticSamplerDesc.BorderColor = D3D12_STATIC_BORDER_COLOR_OPAQUE_WHITE;
			else
				staticSamplerDesc.BorderColor = D3D12_STATIC_BORDER_COLOR_OPAQUE_BLACK;
		}
		else
			staticSamplerDesc.BorderColor = D3D12_STATIC_BORDER_COLOR_TRANSPARENT_BLACK;
	}
}

void IRootSignature::Finalize(D3D12_ROOT_SIGNATURE_FLAGS flags) {
	if (m_bFinalized) return;

	D3D12_ROOT_SIGNATURE_DESC rootDesc;
	rootDesc.NumParameters = m_uiNumOfParameters;
	rootDesc.pParameters = (const D3D12_ROOT_PARAMETER*)m_pParameterLists.get();
	rootDesc.Flags = flags;
	rootDesc.NumStaticSamplers = m_uiNumOfStaticSamplers;
	rootDesc.pStaticSamplers = (const D3D12_STATIC_SAMPLER_DESC*)m_pStaticSamplerArr.get();

	size_t HashCode = HelperFuncs::HashStateArray(rootDesc.pStaticSamplers, m_uiNumOfStaticSamplers);
	// TODO need to test this, create three root signatures, two of them are identical
	for (unsigned int param = 0; param < m_uiNumOfParameters; param++) {
		const D3D12_ROOT_PARAMETER& rootParam = rootDesc.pParameters[param];
		m_uiDescriptorTableSize[param] = 0;

		if (rootParam.ParameterType == D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE) {
			if (rootParam.DescriptorTable.pDescriptorRanges == nullptr) HelperFuncs::ErrorMsgBox(L"Root parameter range cannot be zero.");

			HashCode = HelperFuncs::HashStateArray(rootParam.DescriptorTable.pDescriptorRanges,
				rootParam.DescriptorTable.NumDescriptorRanges, HashCode);

			// Static Sampler is not managed in the Mini Engine's DescriptorCache
			if (rootParam.DescriptorTable.pDescriptorRanges->RangeType == D3D12_DESCRIPTOR_RANGE_TYPE_SAMPLER) continue;

			for (unsigned int tableRange = 0; tableRange < rootParam.DescriptorTable.NumDescriptorRanges; ++tableRange)
				m_uiDescriptorTableSize[param] += rootParam.DescriptorTable.pDescriptorRanges[tableRange].NumDescriptors;
		}
		else {
			HashCode = HelperFuncs::HashState(&rootParam, HashCode);
		}
	}

	ID3D12RootSignature** RSRef = nullptr;
	bool firstCompile = false;
	{
		EnterCriticalSection(&s_HashMapCriSection);
		auto iter = s_gRootSignatureHashMap.find(HashCode);

		if (iter == s_gRootSignatureHashMap.end()) {
			// If the new root signature is not found in the hash map,
			// we gonna generate a new root signature with RSRef
			RSRef = s_gRootSignatureHashMap[HashCode].GetAddressOf();
			firstCompile = true;
		}
		else {
			// If we found the root signature has been created,
			// we just get its reference and return it
			RSRef = iter->second.GetAddressOf();
		}

		LeaveCriticalSection(&s_HashMapCriSection);
	}

	if (firstCompile) {
		ComPtr<ID3DBlob> signature;
		ComPtr<ID3DBlob> error;

		if (FAILED(D3D12SerializeRootSignature(&rootDesc, D3D_ROOT_SIGNATURE_VERSION_1, &signature, &error))) {
			if (error) {
				OutputDebugStringA(reinterpret_cast<const char*>(error->GetBufferPointer()));
				error = nullptr;
			}
			throw;
		}
		if (FAILED(g_pDevice->CreateRootSignature(0, signature->GetBufferPointer(), signature->GetBufferSize(), IID_PPV_ARGS(&m_pRootSignature)))) {
			if (error) {
				OutputDebugStringA(reinterpret_cast<const char*>(error->GetBufferPointer()));
				error = nullptr;
			}
			throw;
		}

		s_gRootSignatureHashMap[HashCode].Attach(m_pRootSignature.Get());

		// Check if the RSRef == the new root signature
		// Make sure there is not collision in the hash map
		ASSERT((*RSRef == m_pRootSignature.Get()));
	}
	else {
		while (*RSRef == nullptr) this_thread::yield();
		m_pRootSignature = *RSRef;
	}
	m_bFinalized = true;
}


//****************************************//
// Function definitions of IRootParameter
//****************************************//
IRootParameter::IRootParameter() {
	m_rootParam.ParameterType = (D3D12_ROOT_PARAMETER_TYPE)-1;
}

IRootParameter::~IRootParameter() {
	CleanUp();
}

void IRootParameter::CleanUp() {
	if (m_rootParam.ParameterType == D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE) {
		delete[] m_rootParam.DescriptorTable.pDescriptorRanges;

		m_rootParam.ParameterType = (D3D12_ROOT_PARAMETER_TYPE)-1;
	}
}

void IRootParameter::InitAsDescriptorTable(unsigned int rangeCount, D3D12_SHADER_VISIBILITY visibility) {
	m_rootParam.ParameterType = D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE;
	m_rootParam.ShaderVisibility = visibility;
	m_rootParam.DescriptorTable.NumDescriptorRanges = rangeCount;
	m_rootParam.DescriptorTable.pDescriptorRanges = new D3D12_DESCRIPTOR_RANGE[rangeCount];
}

void IRootParameter::SetDescriptorTableRange(unsigned int rangeIdx, D3D12_DESCRIPTOR_RANGE_TYPE rangeType, unsigned int registerID, unsigned int count, unsigned int space) {
	D3D12_DESCRIPTOR_RANGE* range = const_cast<D3D12_DESCRIPTOR_RANGE*>(m_rootParam.DescriptorTable.pDescriptorRanges + rangeIdx);
	range->BaseShaderRegister = registerID;
	range->NumDescriptors = count;
	range->OffsetInDescriptorsFromTableStart = D3D12_DESCRIPTOR_RANGE_OFFSET_APPEND;
	range->RangeType = rangeType;
	range->RegisterSpace = space;
}

void IRootParameter::InitAsDescriptorRange(D3D12_DESCRIPTOR_RANGE_TYPE type, unsigned int registerId, unsigned int count, D3D12_SHADER_VISIBILITY visibility) {
	InitAsDescriptorTable(1, visibility);
	SetDescriptorTableRange(0, type, registerId, count);
}

void IRootParameter::InitAsConstants(unsigned int registerId, unsigned int numOfDwords, D3D12_SHADER_VISIBILITY visibility) {
	m_rootParam.ShaderVisibility = visibility;
	m_rootParam.ParameterType = D3D12_ROOT_PARAMETER_TYPE_32BIT_CONSTANTS;
	m_rootParam.Constants.Num32BitValues = numOfDwords;
	m_rootParam.Constants.RegisterSpace = 0;
	m_rootParam.Constants.ShaderRegister = registerId;
}

void IRootParameter::InitAsConstantsBuffer(unsigned int registerId, D3D12_SHADER_VISIBILITY visibility) {
	m_rootParam.ParameterType = D3D12_ROOT_PARAMETER_TYPE_CBV;
	m_rootParam.ShaderVisibility = visibility;
	m_rootParam.Descriptor.RegisterSpace = 0;
	m_rootParam.Descriptor.ShaderRegister = registerId;
}

void IRootParameter::InitAsBufferSRV(unsigned int registerId, D3D12_SHADER_VISIBILITY visibility) {
	m_rootParam.ParameterType = D3D12_ROOT_PARAMETER_TYPE_SRV;
	m_rootParam.ShaderVisibility = visibility;
	m_rootParam.Descriptor.RegisterSpace = 0;
	m_rootParam.Descriptor.ShaderRegister = registerId;
}

void IRootParameter::InitAsBufferUAV(unsigned int registerId, D3D12_SHADER_VISIBILITY visibility) {
	m_rootParam.ParameterType - D3D12_ROOT_PARAMETER_TYPE_UAV;
	m_rootParam.ShaderVisibility = visibility;
	m_rootParam.Descriptor.RegisterSpace = 0;
	m_rootParam.Descriptor.ShaderRegister = registerId;
}

